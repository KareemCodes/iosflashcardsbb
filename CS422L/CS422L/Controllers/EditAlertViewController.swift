//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: Flashcard = Flashcard()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        self.dismiss(animated: true, completion: {
            let deletedCard = self.parentVC.cards[self.parentVC.selectedIndex]
            
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            //delete the card and tell the tableview to reload
            
            self.parentVC.selectedSet?.removeFromFlashcards(deletedCard)
            managedContext.delete(deletedCard)
            self.parentVC.cards.remove(at: self.parentVC.selectedIndex)
            self.parentVC.tableView.reloadData()
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        })
    
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
                return
            }

            let managedContext =
            appDelegate.persistentContainer.viewContext
            
            
            do {
                // save changes
                self.parentVC.cards[self.parentVC.selectedIndex].term = self.termEditText.text ?? ""
                self.parentVC.cards[self.parentVC.selectedIndex].definition = self.definitionEditText.text ?? ""
                try managedContext.save()
                self.parentVC.tableView.reloadData()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }

            //do something / save the edits
        })
    }
    
}
